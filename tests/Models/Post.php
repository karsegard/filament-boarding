<?php

namespace KDA\Tests\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class Post extends Model 
{
   
    use \KDA\Sluggable\Models\Traits\HasSlugs;
    use HasFactory;

    protected $fillable = [
        'title'
    ];

    public function slugCollectionName(){
        return 'Posts';
    }

    public function getSluggableAttribute(){
        return $this->title;
    }
    protected static function newFactory()
    {
        return  \KDA\Tests\Database\Factories\PostFactory::new();
    }
}
