<?php

namespace KDA\Filament\Boarding;
use Filament\PluginServiceProvider;
use Spatie\LaravelPackageTools\Package;

class FilamentServiceProvider extends PluginServiceProvider
{
    protected array $styles = [
    //    'my-package-styles' => __DIR__ . '/../dist/app.css',
    ];

    protected array $widgets = [
    //    CustomWidget::class,
    ];

    protected array $pages = [
    //    CustomPage::class,
    ];

    protected array $resources = [
   //     CustomResource::class,
    ];
 
    public function configurePackage(Package $package): void
    {
        $package->name('filament-boarding');
    }
    public function registeringPackage()
    {
        parent::registeringPackage();
        $this->app['config']->push('filament.middleware.auth', BoardingMiddleware::class);
    }
 
    public function packageBooted(): void
    {
        parent::packageBooted();
       
    }
}
