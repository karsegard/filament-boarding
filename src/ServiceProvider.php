<?php
namespace KDA\Filament\Boarding;
use KDA\Laravel\PackageServiceProvider;
//use Illuminate\Support\Facades\Blade;
class ServiceProvider extends PackageServiceProvider
{
    use \KDA\Laravel\Traits\HasCommands;
    use \KDA\Laravel\Traits\HasProviders;
    use \KDA\Laravel\Traits\HasViews;


    protected $packageName = 'filament-boarding';

    /* This is mandatory to avoid problem with the package path */
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
        //register filament provider
    protected $additionnalProviders=[
        \KDA\Filament\Boarding\FilamentServiceProvider::class
    ];
    public function register()
    {
        $this->app->singleton('kda.filament-boarding', function ($app) {
            return new Boarding();
        });
        parent::register();
  
      
    }
    /**
     * called after the trait were registered
     */
    public function postRegister(){
    }
    //called after the trait were booted
    protected function bootSelf(){
    }
}
