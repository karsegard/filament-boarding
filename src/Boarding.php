<?php

namespace KDA\Filament\Boarding;

use Closure;

class BoardingTrack {

    protected $steps;

    public function __construct($steps){
        $this->steps = $steps;
    }

    public static function make($steps): static
    {
        $static = app(static::class, ['steps'=>$steps]);
        return $static;
    }

    public function completed(){
        $completed = true;
        foreach($this->steps as $step){
            if(!$step->completed()){
                $completed = false;
                break;
            }
        }
        return $completed;
    }

    public function firstUncompletedStep():?BoardingStep{
        foreach($this->steps as $step){
            if(!$step->completed()){
                return $step;
            }
        }
        return null;
    }
   
}

class BoardingStep {
    protected $redirect ;
    protected $completeCondition;
    protected $wizard;
    public static function make($identifier): static
    {
        $static = app(static::class, ['identifier'=>$identifier]);
        return $static;
    }

    public function completeIf(Closure $callback){
        $this->completeCondition = $callback;
        return $this;
    }
    public function completed(){
        $callback = $this->completeCondition;
        if(!$callback){
            return true;
        }
        return $callback();
    }

    public function redirect($url){
        $this->redirect = $url;
        return $this;
    }

    public function getRedirect(){
        return $this->redirect;
    }

}

class Boarding {

    protected $tracks = [];
    protected $route_exceptions = [];
    public function getTracks(){
        return $this->tracks;
    }
    public function addTrack(array $steps): BoardingTrack{
        $track=  BoardingTrack::make($steps);
        $this->tracks [] = $track;
        return $track;
    }

    public function addStep($name,$identifier):BoardingStep{
        return BoardingStep::make($name,$identifier);
    }

    public function exceptRoutes(string | array $routes):static{
        if(!is_array($routes)){
            $routes = [$routes];
        }
        $this->route_exceptions = array_merge($this->route_exceptions,$routes);
        return $this;
    }
}


