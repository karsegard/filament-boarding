<?php

namespace KDA\Filament\Boarding;

use Closure;
use Illuminate\Http\Request;
use KDA\Filament\Boarding\Facades\Boarding;

class BoardingMiddleware
{
    protected array | null $except;

    public function __construct()
    {
        $this->except = config('kda.filament-boarding.middleware.exceptions', [ '/filament/*','livewire/*','/admin/login','/admin/logout']);
    }

    protected function inExceptArray($request)
    {
        foreach ($this->except as $except) {
            if ($except !== '/') {
                $except = trim($except, '/');
            }

            if ($request->fullUrlIs($except) || $request->is($except)) {
                return true;
            }
        }

        return false;
    }

    public function handle(Request $request, Closure $next)
    {
        $tracks = Boarding::getTracks();
        if (!$this->inExceptArray($request)) {
            foreach ($tracks as $track) {

                if (!$track->completed()) {
                    $step = $track->firstUncompletedStep();
                    $url = trim($step->getRedirect(), '/');
                    if (!$request->fullUrlIs($url) &&  !$request->is($url)) {
                        return redirect($url);
                    }
                }
            }
        }
        return $next($request);
    }
}
