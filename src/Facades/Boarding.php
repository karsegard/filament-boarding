<?php
namespace KDA\Filament\Boarding\Facades;
use Illuminate\Support\Facades\Facade;

class Boarding extends Facade
{
   
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'kda.filament-boarding'; }
}