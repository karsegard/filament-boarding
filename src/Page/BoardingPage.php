<?php

namespace KDA\Filament\Boarding\Page;


use Filament\Pages\Page;
use Filament\Pages\Concerns\HasFormActions;
use Filament\Pages\Contracts\HasFormActions as FormActions;

class BoardingPage extends Page implements FormActions
{
    use HasFormActions;

    protected static string $layout = 'filament-boarding::layouts.app-single';
    
    protected static function shouldRegisterNavigation(): bool
    {
        //return auth()->user()->canManageSettings();
        return false;
    }
    
    protected function getLayoutData(): array
    {
        return [
            ...parent::getLayoutData(),
            'maxContentWidth'=>'2xl'
        ];
    }


}
